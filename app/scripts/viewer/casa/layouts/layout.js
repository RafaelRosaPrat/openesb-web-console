function Layout() {};

/**
 * Count number of incoming et outcoming connection 
 */
Layout.connectionCount= function(eps){
	var res = { in : 0, out : 0};

	$(eps).each(function(idx, ep){
		if(ep.connections){
     		if(ep.isSource){
				res.out += ep.connections.length;
     		}else{
     			res.in += ep.connections.length; 
     		}

     		//case strange to test
     		//if(ep.element.id == "f58ba91680f0c9981a1ba7f0d687bab9-ListeCourseWS"){
     		//	console.log(ep.id);
     		//	console.log(res);
     		//}
     		if(ep.connections.length == 0){
				$(ep.endpoint.canvas).hide();
			}
     	}
     });
     return res;
};


Layout.getPonderations =function (eps){
	
	var res = { ponderation : 0, nbConnections:0}
	$(eps).each(function(idx, ep){
		epRes = Layout.getPonderation(ep);
		res.ponderation += epRes.ponderation ;
		res.nbConnections += epRes.nbConnections;
     });
     return res.ponderation / res.nbConnections;
};

/**
 * Compute a ponderation to determinate position next.
 *
 * @param ep 
 * @param isSource
 */
Layout.getPonderation = function(ep, isSource){
	
	var res = { ponderation : 0, nbConnections:0}
	if(ep.connections){
     		if(ep.isSource){
     			$(ep.connections).each(function(idx, c){
     				res.ponderation +=c.endpoints[1].anchor.y;
     				res.nbConnections  += 1;
     			});
     		}else{
     			$(ep.connections).each(function(idx, c){
     				res.ponderation +=c.endpoints[0].anchor.y;
     				res.nbConnections  += 1;
     			});
     		}
     }
     return res;
};

/**
 * Position elements to display
 *
 * selector : Seletor to get current view
 **/
Layout.apply = function(selector){
	//return;


	var offsetX = $(selector).position().left;
	var offsetY =$(selector).position().top;
	//alert("x=" +offsetX+ " y="+offsetY);


	var height = $(selector).height();
	var width = $(selector).width();
	console.debug("dimension = ["+width+","+height+"]" );
	
	var maxBpelWith = 0;
	var minBorderSpace = width;




	//Step 1 : Define bpel modules position
	var bpelMargin = 20;
	var bpelTop = firstBpelTop = 70;
	$(".service-unit").each(function(pos, elt){

		//if($(elt).is(':visible')){
			positionRight = positionLeft = ((width-$(elt).outerWidth()) / 2);
			if(positionLeft < minBorderSpace){
				minBorderSpace = positionLeft;
			}
			
			bpelWith = $(elt).outerWidth()
			if( maxBpelWith < bpelWith){
				maxBpelWith = bpelWith;
			}

			$(elt).css({top: bpelTop, left: offsetX+positionLeft});
			instance.repaint($(elt));
			bpelTop =  bpelTop+$(elt).outerHeight()+bpelMargin;		
		//}


	});
	

	
	var shiftToIn = 10;
	var shiftToOut = 10;

	var connectorLeftMax = 0;
	var connectorRightMax = 0;


	

	//Step : computing connectorMax Width 
	$(".connector").each(function(pos, elt){ 
		var eps = instance.getEndpoints($("#"+elt.id)); 
		var nbConnections = Layout.connectionCount(eps);
		var connectorWidth = $("#"+elt.id).outerWidth();
		if(nbConnections.out > 0){
			if(connectorWidth > connectorLeftMax){
				connectorLeftMax = connectorWidth;
			}
		}else{
			if(connectorWidth > connectorRightMax){
				connectorRightMax = connectorWidth;
			}
		}
	});

    var leftRepere = offsetX+ (minBorderSpace - connectorLeftMax )/2+connectorLeftMax;
    var rightRepere = offsetX+(minBorderSpace) + (maxBpelWith) + ((minBorderSpace -connectorRightMax)/2) ;

	console.log(minBorderSpace);

	//Step 2 : Define connnector position
	$(".connector").each(function(pos, elt){ 

		var eps = instance.getEndpoints($("#"+elt.id)); 
		var nbConnections = Layout.connectionCount(eps);

		var shiftOutLeft = leftRepere - $("#"+elt.id).outerWidth();
		if(nbConnections.out > 0){
			$("#"+elt.id).css({top: shiftToOut, left: shiftOutLeft});
			shiftToOut+= (10+ $("#"+elt.id).outerHeight());
		}else{
			$("#"+elt.id).css({top: shiftToIn, left: rightRepere});
			shiftToIn+= (10+ $("#"+elt.id).outerHeight());
		}
		
		instance.repaint($("#"+elt.id), (nbConnections.out > 0));

		//compute connection pr
		var pond = Layout.getPonderations(eps);
		if(pond){
			$("#"+elt.id).css({top: (pond * $(".service-unit").height()) + firstBpelTop-15});
		}
	});
};



