'use strict';

openesbApp.directive('formDirective', function() {
    return {
        controller: ['$scope',
            function($scope) {
                $scope.submit = function() {
                    //TODO: how to better handle this ?
                    $scope.$parent.saveForm();
                }
            }
        ],
        templateUrl: 'views/directives/form/form.html',
        restrict: 'E',
        scope: {
            form: '=',
            saveEnabled: '='
        }
    };
});