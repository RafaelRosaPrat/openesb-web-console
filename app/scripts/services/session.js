'use strict';

openesbApp.factory('Session', function () {
    this.create = function (username, token) {
        this.username = username;
        this.token = token;
    };

    this.invalidate = function () {
        this.username = null;
        this.token = null;
    };

    return this;
});