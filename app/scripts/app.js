'use strict';

/* App Module */
var openesbApp = angular.module('openesbApp', [
        'ui.bootstrap',
        'ngRoute',
        'ngResource',
        'ngCookies',
        'ngSanitize',
        'blueimp.fileupload',
        'ui.router',
        'toaster',
        'http-interceptor',
        'http-auth-interceptor',
        'openesb.instance',
        'openesb.assemblies',
        'openesb.components',
        'openesb.libraries',
        'openesb.endpoints',
        'openesb.serviceunits'
    ]).constant('config', {
        refresh_interval: 1000
    }).config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
        function($stateProvider, $urlRouterProvider, $httpProvider) {
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

            $urlRouterProvider
                .otherwise('/');

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: 'views/home.html',
                    controller: 'HomeCtrl'
                })
                .state('login', {
                    url: '/login',
                    templateUrl: 'views/login.html',
                    controller: 'LoginCtrl'
                })
                .state('logout', {
                    url: '/logout',
                    controller: 'LogoutCtrl'
                });
        }])
    .config([
        '$httpProvider', 'fileUploadProvider',
        function ($httpProvider, fileUploadProvider) {
            delete $httpProvider.defaults.headers.common['X-Requested-With'];

            fileUploadProvider.defaults.redirect = window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
            );
            angular.extend(fileUploadProvider.defaults, {
                maxNumberOfFiles: 1,
                acceptFileTypes: /(\.|\/)(jar|zip)$/i,
                replaceFileInput: true
            });
        }
    ]).config([
        'toasterConfig',
        function(toasterConfig) {
            toasterConfig['limit'] = 3;
        }
    ]).run([
        '$rootScope', '$location', '$state', '$stateParams', 'Session',
        function($rootScope, $location, $state, $stateParams, Session) {
            // Call when a new component is installed
            $rootScope.$on('event:component-installed', function(event, component) {
                $location.path('/components/' + component + '/configuration').replace();
            });

            // Call when a new service assembly is installed
            $rootScope.$on('event:assembly-installed', function(event, component) {
                $location.path('/assemblies/' + component).replace();
            });

            // Call when a new shared library is installed
            $rootScope.$on('event:library-installed', function(event, component) {
                $location.path('/libraries/' + component).replace();
            });

            // Call when the 401 response is returned by the client
            $rootScope.$on('event:auth-loginRequired', function(rejection) {
                Session.invalidate();
                $rootScope.authenticated = false;
                $location.path('/login').replace();
            });

            // Call when the the client is confirmed
            $rootScope.$on('event:auth-loginConfirmed', function(data) {
                $rootScope.authenticated = true;
                if ($location.path() === '/login') {
                    $location.path('/').replace();
                }
            });

            // Call when the user logs out
            $rootScope.$on('event:auth-loginCancelled', function() {
                $location.path('');
            });

            // Debug mode
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }
    ]);
