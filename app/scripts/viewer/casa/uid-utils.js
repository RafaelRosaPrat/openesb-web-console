function UIDUtils() {};

/**
* Add UID to the given connection point.
*
* @param cp Connection point
**/ 
UIDUtils.initConnectionPointUID = function(cp){
	var key = cp.endpointName;
	if(cp.serviceName){
		key +=  "{"+ cp.serviceName.namespaceURI +"}"+ cp.serviceName.localPart;
	}
	cp.uid = CryptoJS.MD5(key).toString();
};


/**
 * Compute UIDs for all model's connection point.
 *
 * @param model Model to initialize
 */
UIDUtils.initModelUIDs = function(model){
	console.debug("Begin assign UIDs");
	if(model){
		//Add assemblies UIDs
		if(model.serviceUnits){
			$.each(model.serviceUnits, function( idxCasa, casa ) {
				$.each(casa.processes, function( idxProcess, process ) {
					$.each(process.inputs, function( idxInput, input ) {
						UIDUtils.initConnectionPointUID(input.connectionPoint);
					});
					$.each(process.outputs, function( idxOutput, output ) {
						UIDUtils.initConnectionPointUID(output.connectionPoint);
					});
				});
			});
		}
		console.debug("Begin assign connectors UIDs");

		//Add connectors UIDs
		if(model.connectors){
			$.each(model.connectors, function( idxConnector, connector ) {
				UIDUtils.initConnectionPointUID(connector.connectionPoint);	
			});
		}

		console.debug("Begin assign models UIDs");
		//Add link endpoint UIDs
		if(model.links){
			$.each(model.links, function( idxLink, link ) {
				UIDUtils.initConnectionPointUID(link.source);
				UIDUtils.initConnectionPointUID(link.target);	
			});
		}
	}
	console.debug("End assign UIDs");
	return model;
};

