# openesb-web-console

The openeb web console is used to visualize and manage 
[OpenESB standalone Server](https://bitbucket.org/openesb/openesb-standalone/src/master/).

## To build (Ubuntu 16.04)
### 1. Install nvm (to allow multiple node version)

Install the latest nvm as explained [here](https://github.com/creationix/nvm). 
On the 03/05/2018 with v0.33.11 the following was done
 
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
source ~/.bashrc
```

### 2. Install node v4.9.1

```
nvm install v4.9.1
```

### 3. From openesb-web-console root

This section assumes that you've cloned the source from bitbucket (cloning source is out of scope). 
From the source root folder do the following

```
npm install
npm install -g bower
npm install -g grunt
$MAVEN_HOME/bin/mvn clean package
```
 

