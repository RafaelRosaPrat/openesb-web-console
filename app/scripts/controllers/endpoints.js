'use strict';

var openesbEndpoints = angular.module('openesb.endpoints', [
        'ui.router'
    ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $stateProvider
                    .state('endpoints', {
                        url: '/endpoints',
                        templateUrl: 'views/endpoints/endpoints.html',
                        controller: 'EndpointsListCtrl'
                    });
            }
        ]
    );

openesbApp.controller('EndpointsListCtrl', ['$scope', 'EndpointService',
    function ($scope, EndpointService) {

        EndpointService.findAll()
            .success(function(data) {
                $scope.endpoints = data;
            });

        $scope.predicate = 'name';
    }
]);