'use strict';

openesbApp.controller('MainCtrl', ['$scope', '$route', '$http', '$q','$location', '$rootScope', '$window', 'toaster', 'InstanceService',
    function($scope, $route, $http, $q, $location, $rootScope, $window, toaster, InstanceService) {

        // Call when an error response is returned by the server
        $scope.$on('event:http-response-error', function(event, rejection) {
            toaster.pop('warning', rejection.status, rejection.data);
        });

        // Listen for changes to the state. When the state
        // changes, let's set the renderAction model value so
        // that it can render in the Strong element.
        $scope.$on(
            "$stateChangeStart",
            function() {

                if ($location.path() !== undefined) {
                    $scope.isAssemblies = ($location.path().indexOf("/assemblies") === 0);
                    $scope.isComponents = ($location.path().indexOf("/components") === 0);
                    $scope.isLibraries = ($location.path().indexOf("/libraries") === 0);
                    $scope.isEndpoints = ($location.path().indexOf("/endpoints") === 0);
                    $scope.isMonitor = ($location.path().indexOf("/monitor") === 0);
                    $scope.isInstance = ($location.path().indexOf("/instance") === 0);
                    $scope.isDashboard = !($scope.isAssemblies || $scope.isComponents || $scope.isLibraries || $scope.isEndpoints ||
                        $scope.isMonitor || $scope.isInstance);
                }

                // TODO: Check the remote instance availability
                InstanceService.checkAvailability();
            }
        );

        $scope.init = function() {
            if ($window.localStorage.token !== undefined) {
                $rootScope.authenticated = true;
            }
        };

        $scope.init();
    }
]);

openesbApp.controller('LanguageCtrl', ['$scope', '$translate',
    function ($scope, $translate) {
        $scope.changeLanguage = function (languageKey) {
            $translate.uses(languageKey);
        };
    }
]);

openesbApp.controller('LoginCtrl', ['$scope', '$location', '$timeout', 'AuthenticationService',
    function ($scope, $location, $timeout, AuthenticationService) {
        document.getElementById("username").focus();

        $scope.login = function (form) {
            // Trigger validation flag.
            $scope.submitted = true;

            // If form is invalid, return and let AngularJS show validation errors.
            if (form.$invalid) {
                return;
            }

            AuthenticationService.login(
                $scope.username,
                $scope.password
            );

            // Hide the status message which was set above after 3 seconds.
            $timeout(function() {
                $scope.authenticationError = false;
            }, 3000);
        }
    }
]);

openesbApp.controller('LogoutCtrl', ['$location', 'AuthenticationService',
    function ($location, AuthenticationService) {
        AuthenticationService.logout();

        $location.path('/login').replace();
    }
]);
