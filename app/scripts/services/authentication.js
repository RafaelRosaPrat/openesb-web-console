'use strict';

openesbApp.service('AuthenticationService', ['$http', '$rootScope', '$cookieStore', 'authService', '$window', 'Session',
  function($http, $rootScope, $cookieStore, authService, $window, Session) {
    this.login = function(username, password) {
      var encodedUserNameAndPassword = Base64.encode(username + ':' + password);

      $http({
        method: 'POST',
        url: '/login',
        headers: { 'Authorization': 'Basic ' + encodedUserNameAndPassword },
        ignoreAuthModule: 'ignoreAuthModule'
      }).success(function (data, status, headers, config) {
        $cookieStore.put('basicCredentials', encodedUserNameAndPassword);
        $http.defaults.headers.common['Authorization'] = 'Basic ' + encodedUserNameAndPassword;

        $rootScope.username = username;
        authService.loginConfirmed(data);
      }).error(function (data, status, headers, config) {
        //    $cookieStore.remove('basicCredentials');
        //    delete $http.defaults.headers.common['Authorization'];
        $rootScope.authenticationError = true;
      });
    };

    this.logout = function() {
      $cookieStore.remove('basicCredentials');
      delete $http.defaults.headers.common['Authorization'];
    };
  }
]);
