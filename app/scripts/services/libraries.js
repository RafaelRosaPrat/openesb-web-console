'use strict';

openesbApp.service('LibraryService', ['$http', '$q',
    function($http, $q) {
        this.findAll = function() {
            return $http.get('/libraries');
        };

        this.get = function(libraryName) {
            return $http.get('/libraries/' + libraryName);
        };

        this.getDescriptorAsXml = function(libraryName) {
            var fullResult = $q.defer();

            $http.get(
                '/libraries/' + libraryName + '/descriptor',
                {
                    headers: {'Accept': 'application/xml'}
                }
            ).success(function(response) {
                    var descriptor = {};
                    descriptor.xml = _.escape(response.toString());
                    fullResult.resolve(descriptor);
                });

            return fullResult.promise;
        };

        this.delete = function(libraryName) {
            return $http.delete('/libraries/' + libraryName + '?force=true');
        };
    }
]);