'use strict';

openesbApp.service('ServiceUnitService', ['$http', '$q', 'ComponentService',
    function($http, $q, ComponentService) {

        function constructUrl(componentType, componentName) {
            return '/' + componentType + '/' + componentName + '/';
        };

        this.get = function(componentType, componentName, serviceUnitName) {
            var result = $q.defer();
            var url = constructUrl(componentType, componentName);

            $http.get(url).success(function(response) {
                var serviceUnit = _.find(response.serviceUnits, function(serviceUnit){
                    return serviceUnit.name === serviceUnitName;
                });

                result.resolve(serviceUnit);
            });

            return result.promise;
        };

        this.getDescriptorAsXml = function(componentType, componentName, serviceUnitName) {
            var result = $q.defer();

            if (componentType === 'components') {
                ComponentService.get(componentName).success(function(component) {
                    var serviceUnit = _.find(component.serviceUnits, function(serviceUnit){
                        return serviceUnit.name === serviceUnitName;
                    });

                    var url = '/assemblies/' + serviceUnit.serviceAssembly + '/descriptor?su=' + serviceUnitName;
                    $http.get(url,
                        {
                            headers: {'Accept': 'application/xml'}
                        }
                    ).success(function(response) {
                            var descriptor = {};
                            descriptor.xml = _.escape(response.toString());
                            result.resolve(descriptor);
                        });
                });
            } else {
                var url = '/assemblies/' + componentName + '/descriptor?su=' + serviceUnitName;
                $http.get(url,
                    {
                        headers: {'Accept': 'application/xml'}
                    }
                ).success(function(response) {
                        var descriptor = {};
                        descriptor.xml = _.escape(response.toString());
                        result.resolve(descriptor);
                    });
            }

            return result.promise;
        };
    }
]);