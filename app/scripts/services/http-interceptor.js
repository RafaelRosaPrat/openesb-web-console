(function () {
  'use strict';

  angular.module('http-interceptor', [])
    .config(['$httpProvider', function ($httpProvider) {
      $httpProvider.interceptors.push(['$rootScope', '$q', 'Datastore', '$window', function ($rootScope, $q, Datastore, $window) {
        return {
          'request': function (config) {
            if (config.url.indexOf('/') === 0) {
              config.url = Datastore.getCurrentInstance().url + config.url;

              /*
               if (config.headers['Authorization'] === undefined && $window.localStorage.token) {
               config.headers['Authorization'] = 'Basic ' + $window.localStorage.token;
               }
               */
            }
            return config || $q.when(config);
          }
        };
      }]);
    }]);
})();
