'use strict';

openesbApp.service('MonitorService', ['$http', '$q',
    function($http, $q) {
        this.getJVMMetrics = function() {
            return $q.all([
                    $http.get('/jvm/gc'),
                    $http.get('/jvm/memory'),
                    $http.get('/jvm/thread')]);
        };
    }
]);