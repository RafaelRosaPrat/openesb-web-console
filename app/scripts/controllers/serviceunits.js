'use strict';

var openesbServiceUnits = angular.module('openesb.serviceunits', [
        'ui.router'
    ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider
                    .when('/:componentType/:componentName/units/:unitName', '/:componentType/:componentName/units/:unitName/general');

                $stateProvider
                    .state('serviceunit', {
                        url: '/:componentType/:componentName/units/:unitName',
                        views: {
                            "@" : {
                                abstract: true, // Is it really needed ?
                                templateUrl: 'views/serviceunits/serviceunit.html',
                                controller: 'ServiceUnitDetailCtrl'
                            }
                        }
                    })
                    .state('serviceunit.descriptor', {
                        url: '/descriptor',
                        templateUrl: 'views/serviceunits/serviceunit-descriptor.html',
                        controller:['$scope', 'resolvedDescriptor', function($scope, resolvedDescriptor) {
                            $scope.descriptor = resolvedDescriptor.xml;
                        }],
                        resolve: {
                            resolvedDescriptor:['$stateParams', 'ServiceUnitService', function ($stateParams, ServiceUnitService) {
                                return ServiceUnitService.getDescriptorAsXml($stateParams.componentType, $stateParams.componentName, $stateParams.unitName);
                            }]
                        }
                    })
                    .state('serviceunit.general', {
                        url: '/general',
                        templateUrl: 'views/serviceunits/serviceunit-general.html',
                        controller: 'ServiceUnitDetailCtrl'
                    });
            }
        ]
    );

openesbServiceUnits.controller('ServiceUnitDetailCtrl', ['$scope', '$state', '$stateParams', 'ServiceUnitService',
    function ($scope, $state, $stateParams, ServiceUnitService) {

        var type = $stateParams.componentType;
        var componentName = $stateParams.componentName;

        $scope.component = {};
        $scope.component.type = type;
        $scope.component.name = componentName;

        $scope.serviceunit = ServiceUnitService.get(type, componentName, $stateParams.unitName)
            .then(function(data) {
                $scope.serviceunit = data;
            }, function (err) {
                $scope.error = data;
            });

        $scope.getTabClass = function(tabname) {
            return {
                'active': ($state.current.name === 'serviceunit.'+tabname)
            }
        };
    }
]);