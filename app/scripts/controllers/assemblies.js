'use strict';

var openesbAssemblies = angular.module('openesb.assemblies', [
        'ui.router'
    ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider
                    .when('/assemblies/:assemblyName', '/assemblies/:assemblyName/general');

                $stateProvider
                    .state('assemblies', {
                        url: '/assemblies',
                        templateUrl: 'views/assemblies/assemblies.html',
                        controller: 'AssembliesListCtrl'
                    })
                    .state('assembly-install', {
                        url: '/assembly/install',
                        templateUrl: 'views/assemblies/assembly-install.html'
                    })
                    .state('assembly-view', {
                        url: '/assembly/view/:assemblyName',
                        templateUrl: 'views/assemblies/assembly-view.html',
                        controller:['$scope', '$stateParams', 'resolvedMap', function($scope, $stateParams, resolvedMap) {
                            $scope.map = resolvedMap.data;

                            if($scope.map.assemblies){
                                $scope.map.serviceUnits = $scope.map.assemblies;
                                delete $scope.map.assemblies;
                            }

                            $scope.map = UIDUtils.initModelUIDs($scope.map);
                        }],
                        resolve: {
                            resolvedMap:['$stateParams', 'AssemblyService', function ($stateParams, AssemblyService) {
                                return AssemblyService.getMap($stateParams.assemblyName);
                            }]
                        }
                    })
                    .state('assembly', {
                        url: '/assemblies/:assemblyName',
                        abstract: true, // Is it really needed ?
                        templateUrl: 'views/assemblies/assembly.html',
                        controller: 'AssemblyDetailCtrl'
                    })
                    .state('assembly.general', {
                        url: '/general',
                        templateUrl: 'views/assemblies/assembly-general.html',
                        controller: 'AssemblyGeneralCtrl'
                    })
                    .state('assembly.descriptor', {
                        url: '/descriptor',
                        templateUrl: 'views/assemblies/assembly-descriptor.html',
                        controller:['$scope', 'resolvedDescriptor', function($scope, resolvedDescriptor){
                            $scope.descriptor = resolvedDescriptor.xml;
                        }],
                        resolve: {
                            resolvedDescriptor:['$stateParams', 'AssemblyService', function ($stateParams, AssemblyService) {
                                return AssemblyService.getDescriptorAsXml($stateParams.assemblyName);
                            }]
                        }
                    })
                    .state('assembly.monitoring', {
                        url: '/monitoring',
                        templateUrl: 'views/assemblies/assembly-monitoring.html',
                        controller:['$scope', 'resolvedStatistics', function($scope, resolvedStatistics) {
                            var statistics = resolvedStatistics.data;
                            $scope.statistics = statistics;

                            var lastStartupTime = statistics['LastStartupTime'].value;
                            var upTime = statistics['UpTime (ms)'].value;
                            var startupTime = statistics['StartupTime Avg (ms)'].value;

                            $scope.startupTime = moment(lastStartupTime).format();
                            $scope.uptime = moment.duration(upTime, 'milliseconds');
                            $scope.startup = moment.duration(startupTime, 'milliseconds');

                        }],
                        resolve: {
                            resolvedStatistics:['$stateParams', 'AssemblyService', function ($stateParams, AssemblyService) {
                                return AssemblyService.getStatistics($stateParams.assemblyName);
                            }]
                        }
                    })
                    .state('assembly.view', {
                        url: '/view',
                        templateUrl: 'views/assemblies/assembly-view.html',
                        controller:['$scope', '$stateParams', 'AssemblyService', function($scope, $stateParams, AssemblyService) {
                            $scope.map = null;
                            $scope.serviceUnits = [];
                            $scope.serviceUnit = undefined;
                            $scope.loading = true;


                            AssemblyService.getMap($stateParams.assemblyName).success(function(data) {
                                if(data.assemblies){
                                    data.serviceUnits = data.assemblies;
                                    delete data.assemblies;
                                }

                                $scope.map = UIDUtils.initModelUIDs(data);
                            }).error(function(data) {
                                $scope.error = data;
                            });
                        }]
                    });
            }
        ]
    );

openesbAssemblies.controller('AssemblyUploadController', ['$rootScope', '$scope', 'Datastore', '$cookieStore',
    function ($rootScope, $scope, Datastore, $cookieStore) {
        $scope.options = {
            url: Datastore.getCurrentInstance().url + '/assemblies/',
            dataType: 'text'
        };

        $scope.$on(['fileuploadsend'], function (e, data) {
          data.headers['Authorization'] = 'Basic ' + $cookieStore.get('basicCredentials');
        });

        $('#fileupload').bind('fileuploaddone', function (e, data) {
            var task = JSON.parse(data.result);
            $rootScope.$broadcast('event:assembly-installed', task.component);
        }).bind('fileuploadfail', function (e, data) {
                if (data._response.jqXHR) {
                    $scope.error = data._response.jqXHR.responseText;
                }
            });
    }
]);

openesbAssemblies.controller('AssembliesListCtrl', ['$scope', '$modal', 'AssemblyService', 'toaster',
    function ($scope, $modal, AssemblyService, toaster) {

        $scope.checkboxes = {'checked': false, items: {}};
        // watch for check all checkbox
        $scope.$watch('checkboxes.checked', function(value) {
          angular.forEach($scope.assemblies, function(assembly) {
            if (angular.isDefined(assembly.name)) {
              $scope.checkboxes.items[assembly.name] = value;
            }
          });
        });

        $scope.delete = function(assembly) {
            var modalInstance = $modal.open({
                templateUrl: 'views/assemblies/assembly-delete.html',
                controller: 'DeleteAssemblyInstanceCtrl',
                resolve: {
                    assemblyName: function() {
                        return assembly.name;
                    }
                }
            });

            modalInstance.result.then(function() {
                AssemblyService.delete(assembly.name).success(function(data) {
                    reload();
                    toaster.pop('success', 'Assembly deleted', assembly.name + ' has been deleted successfully.', 3000);
                }).error(function(error) {
                  toaster.pop('error', 'Unable to delete ' + assembly.name, error);
                });
            });
        };

        $scope.startSelected = function() {
          doMultipleAction('start');
        };

        $scope.stopSelected = function() {
          doMultipleAction('stop');
        };

        $scope.shutdownSelected = function() {
          doMultipleAction('shutdown');
        };

        function doMultipleAction(actionName) {

          var assemblies = [];
          angular.forEach($scope.assemblies, function(assembly) {
            if ($scope.checkboxes.items[assembly.name] == true) {
              assemblies.push(assembly.name);
            }
          });

          if (assemblies.length > 0) {
            var promise;
            if (actionName === 'start') {
              promise = AssemblyService.startAssemblies(assemblies);
            } else if (actionName === 'stop') {
              promise = AssemblyService.stopAssemblies(assemblies);
            } else if (actionName === 'shutdown') {
              promise = AssemblyService.shutdownAssemblies(assemblies);
            }

            if (promise !== undefined) {
              promise.then(function(data) {
                reload();
                toaster.pop('success', 'Assemblies ' + actionName, 'Assemblies ' + assemblies + ' have been ' + actionName + 'ed successfully.', 3000);
              }, function(error) {
                toaster.pop('error', 'Unable to perform ' + actionName + ' action for ' + assemblies);
              });
            }
          }
        }

        function reload() {
            uncheckMaster();
            AssemblyService.findAll().success(function(data) {
                $scope.assemblies = data;
            });
        };

        function uncheckMaster() {
          $scope.checkboxes.checked = false;
          angular.forEach($scope.assemblies, function(assembly) {
            if (angular.isDefined(assembly.name)) {
              $scope.checkboxes.items[assembly.name] = false;
            }
          });
        }

        reload();

        $scope.predicate = 'name';
    }
]);

openesbAssemblies.controller('AssemblyDetailCtrl', ['$scope', '$state', '$stateParams', 'AssemblyService',
    function ($scope, $state, $stateParams, AssemblyService) {

        $scope.assembly = {
            name: $stateParams.assemblyName
        };

        AssemblyService.get($stateParams.assemblyName).success(function(data) {
            $scope.assembly = data;
        }).error(function(data) {
                $scope.error = data;
            });

        $scope.getTabClass = function(tabname) {
            return {
                'active': ($state.current.name === 'assembly.'+tabname)
            }
        };

        $scope.$on('event:assembly-lifecycle', function(event, assembly) {
            AssemblyService.get(assembly).success(function(data) {
                $scope.assembly = data;
            }).error(function(data) {
                $scope.error = data;
            });
        });
    }
]);

openesbAssemblies.controller('AssemblyGeneralCtrl', ['$scope', '$location', '$modal', '$stateParams', 'toaster', 'AssemblyService',
    function ($scope, $location, $modal, $stateParams, toaster, AssemblyService) {

        $scope.start = function() {
            AssemblyService.start($scope.assembly.name)
                .success(function(data) {
                toaster.pop('success', 'Assembly started', $scope.assembly.name +
                ' has been started successfully.', 3000);

                //    showError(data);
                    reload();
                })
                .error(function(data) {
                    $scope.error = data;
                });
        };

        $scope.stop = function() {
            AssemblyService.stop($scope.assembly.name)
                .success(function(data) {
                toaster.pop('success', 'Assembly stopped', $scope.assembly.name +
                ' has been stopped successfully.', 3000);

                //    showError(data);
                    reload();
                })
                .error(function(data) {
                    $scope.error = data;
                });
        };

        $scope.shutdown = function() {
            AssemblyService.shutdown($scope.assembly.name)
                .success(function(data) {
                toaster.pop('success', 'Assembly shutdown', $scope.assembly.name +
                ' has been shutdown successfully.', 3000);

                //    showError(data);
                    reload();
                })
                .error(function(data) {
                    $scope.error = data;
                });
        };

      $scope.delete = function() {

        var modalInstance = $modal.open({
          templateUrl: 'views/assemblies/assembly-delete.html',
          controller: 'DeleteAssemblyInstanceCtrl',
          resolve: {
            assemblyName: function() {
              return $scope.assembly.name;
            }
          }
        });

        modalInstance.result.then(function() {
          AssemblyService.delete($scope.assembly.name).success(function(data) {
            $location.path('/assemblies').replace();
            toaster.pop('success', 'Service Assembly deleted', $scope.assembly.name + ' has been deleted successfully.', 3000);
          }).error(function(data) {
            toaster.pop('error', 'Unable to delete ' + $scope.assembly.name, data);
          });
        }, function() {
          console.log('Modal dismissed at: ' + new Date());
        });
      };

        function showError(taskResult) {
          console.log(taskResult);
            var errors = [];

            $.each(taskResult.componentsTask, function(i, compTask) {
                if (compTask.result.taskResult === 'FAILED') {
                    errors.push(compTask);
                }
            });

            $scope.errors = errors;
        }

        function reload() {
            $scope.$emit('event:assembly-lifecycle', $scope.assembly.name);
        }
    }
]);

openesbAssemblies.controller('DeleteAssemblyInstanceCtrl', ['$scope', '$modalInstance', 'assemblyName',
    function ($scope, $modalInstance, assemblyName) {
        $scope.assembly = assemblyName;

        $scope.ok = function() {
            $modalInstance.close($scope.assembly);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);
