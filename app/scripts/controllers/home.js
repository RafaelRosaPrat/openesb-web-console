'use strict';

openesbApp.controller('HomeCtrl', ['$scope', '$route', '$http', '$q','$location','InstanceService',
    function($scope, $route, $http, $q,$location, InstanceService) {

        var instance = $http.get('/');
        var assemblies = $http.get('/assemblies');
        var components = $http.get('/components');
        var libraries = $http.get('/libraries');
        var endpoints = $http.get('/nmr');
        var jvm = $http.get('/jvm');
        var metrics = InstanceService.getJVMMetrics();

        $q.all([instance, assemblies, components, libraries, endpoints, jvm, metrics]).then(function(values) {
            $scope.informations = values[0].data;
            $scope.assemblies = values[1].data;
            $scope.components = values[2].data;
            $scope.libraries = values[3].data;
            $scope.endpointsCount = values[4].data.endpointCount;
            $scope.jvm = values[5].data;
            $scope.gc = values[6][0].data;
            $scope.memory = values[6][1].data;
            $scope.thread = values[6][2].data;
        });
    }
]);
