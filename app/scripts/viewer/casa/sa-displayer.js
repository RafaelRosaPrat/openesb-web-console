var instance = undefined;

function SADisplay() {
    theme = ThemeDefaut;
};

function secureName(name) {
    return name.replace(".", "_");
}

SADisplay.prototype.init = function(config){
    this.config = config;
};


/**
 * Display model
 */
SADisplay.prototype.display = function(model){

    console.log("starting intialise HTML");

    this.config.model = model;

    //Initialise jsplumb mode element
    this.intialiseJSPlumb();
};


SADisplay.prototype.displaySU = function(model){


    if(!model){
        console.log("No model to display");
        return;
    }
    console.log("Starting displaySU");


    $.each(model.serviceUnits, function( idxCasa, casa ) {
        var casaHeight = $("#"+casa.name).height();

        $.each(casa.processes, function( idxProcess, process ) {

            $.each(process.inputs, function( idxInput, input ) {
                //console.log("Search : "+"#"+casa.name+"-"+secureName(process.name)+"-"+input.name+"_in");
                var ep = $($("#"+casa.name+"-"+secureName(process.name)+"-"+input.name+"_in")[0]);
                var sourceUUID = input.connectionPoint.uid + "_in";

                instance.addEndpoint(casa.name ,
                    theme.targetEndpointStyle(input.name),
                    {
                        anchor:[ 0, ((ep.position().top + (ep.height() /2)- theme.puceRadius) / casaHeight), 0, 0 ],
                        uuid:sourceUUID
                    });
            });

            //console.log("Starting process outputs")
            $.each(process.outputs, function( idxOutput, output ) {
                //console.log("Search : "+"#"+casa.name+"-"+secureName(process.name)+"-"+output.name+"_out");
                var ep = $($("#"+casa.name+"-"+secureName(process.name)+"-"+output.name+"_out")[0]);
                var sourceUUID = output.connectionPoint.uid + "_out";
                instance.addEndpoint(casa.name ,
                    theme.sourceEndpointStyle(output.name),
                    { anchor:[ 1, ((ep.position().top+ (ep.height() /2) - theme.puceRadius) / casaHeight), 0, 0 ],
                        uuid:sourceUUID });
            });
        });
    });
    console.log("End displaySU");
};

SADisplay.prototype.displayConnectors = function(model){

    console.log("Starting process connectors")
    console.log(model);
    if(model.connectors){
        $.each(model.connectors, function( idxConnector, connector ) {
            //console.debug("[CI]"+connector.name+"-" + connector.connectionPoint.uid +"_in" );
            var sourceUUID = connector.connectionPoint.uid + "_in";
            //anchor:[ 1, 0.75, 0, 0 ]
            instance.addEndpoint(
                    connector.connectionPoint.uid +"-"+connector.name ,
                theme.targetEndpointStyle(connector.name + "_in"),
                { anchor:["Continuous", { faces:["right", "left"] } ], uuid:sourceUUID });

            //console.debug("[CO]"+connector.name+"-" + connector.connectionPoint.uid +"_out" );
            var sourceUUID = connector.connectionPoint.uid + "_out";
            //anchor:[ 1, 0.25, 0, 0 ]
            instance.addEndpoint(
                    connector.connectionPoint.uid +"-"+connector.name ,
                theme.sourceEndpointStyle(connector.name + "_out"),
                { anchor:["Continuous", { faces:["right", "left"] } ], uuid:sourceUUID });

        });
    }
};

SADisplay.prototype.displayLinks = function(model){
    if(model.links){
        $.each(model.links, function( idxLink, link ) {
            //console.debug(link.source.uid +"_out" +"<-->"+ link.target.uid +"_in" );

            try{
                instance.connect({uuids:[link.source.uid +"_out", link.target.uid +"_in"], editable:true});
            }catch(e){
                console.log(e);
            }
        });
    }
};








SADisplay.prototype.intialiseJSPlumb = function(){

    if(instance){
        instance.reset();
    }


    instance = jsPlumb.getInstance({
        // deault draggableg options
        DragOptions : { cursor: 'pointer', zIndex:2000 },
        // the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
        // case it returns the 'labelText' member that we set on each connection in the 'init' method below.
        ConnectionOverlays : [
            [ "Arrow", { location:1 } ],
            [ "Label", {
                location:0.1,
                id:"label"
            }]
        ],
        Container: $(this.config.container)
    });

    // suspend drawing and initialise.
    instance.doWhileSuspended(function() {


        //Display model
        console.log("starting display");




        this.displaySU(this.config.model);
        this.displayConnectors(this.config.model);
        this.displayLinks(this.config.model);


        //Apply layout go display correctly elements
        Layout.apply(this.config.container);


        // make all the window divs draggable
        instance.draggable(jsPlumb.getSelector(this.config.container +" .window"), { grid: [10, 10],
            stop: function(elt) {
                console.log(elt.target.id);
            }
        });


        $( ".connector" ).mouseover(function(e) {
            var eps = instance.getEndpoints($("#"+e.target.id));
            $(eps).each(function(idx, ep){
                $(ep.connections).each(function(idx, c){
                    c.setPaintStyle(ThemeDefaut.connectorHoverStyle);
                });
            });
        });

        $( ".connector" ).mouseout(function(e) {
            var eps = instance.getEndpoints($("#"+e.target.id));
            $(eps).each(function(idx, ep){
                $(ep.connections).each(function(idx, c){
                    c.setPaintStyle(ThemeDefaut.connectorPaintStyle);
                });
            });
        });

        //---
        // listen for clicks on connections, and offer to delete connections on click.
        //--
        //instance.bind("click", function(conn, originalEvent) {
        //	if (confirm("Delete connection from " + conn.sourceId + " to " + conn.targetId + "?"))
        //		jsPlumb.detach(conn);
        //});


        instance.bind("beforeDrop", function(connection) {

        });

        $(".map-loader").hide();
        //alert("finish");
    }.bind(this));
} 