'use strict';

/* Directives */

openesbApp.directive('prettyprint', function() {
            return {
                restrict: 'C',
                link: function(scope, element) {
                    element.html(prettyPrintOne(scope.descriptor));
                }
            };
        })
        .directive('jbiActions', function() {
            return {
                scope: {
                    item: '=component'
                },
                restrict: 'E',
                link: function(scope, element, attrs) {
                    element.bind('click', function(e) {
                        scope.$apply(attrs.action);
                    });
                },
                /*replace: true,
                 link: function(scope, element, attrs) {
                 alert('toto');
                 scope.ctrlFn();
                 },
                 link: function(scope, element, attrs, componentCtrl) {
                 scope.stopComponent = function() {
                 componentCtrl.stop();
                 };
                 
                 scope.startComponent = function() {
                 alert('start');
                 };
                 
                 scope.shutdownComponent = function() {
                 alert('shutdown');
                 };
                 },
                 controller: function($scope) {
                 
                 $scope.stopComponent = function() {
                 $scope.$apply("stop()");
                 }
                 
                 $scope.startComponent = function() {
                 scope.$apply("start()");
                 }
                 
                 $scope.shutdownComponent = function() {
                 scope.$apply("shutdown()");
                 }
                 },
                 */
                templateUrl: 'views/directives/jbi-actions.html'
            };
        })
        .directive('jbiState', function() {
            return {
                scope: {
                    state: '=state'
                },
                restrict: 'E',
                template: '<span>{{state}}</span>',
                link: function(scope, element) {
                    scope.$watch('state', function(newValue) {
                        if (newValue) {
                            element.attr('class', 'label');

                            if (scope.state === 'STOPPED') {
                                element.addClass("label-warning");
                            } else if (scope.state === 'SHUTDOWN') {
                                element.addClass("label-danger");
                            } else if (scope.state === 'STARTED') {
                                element.addClass("label-success");
                            } else {
                                element.addClass("label-default");
                            }
                        }
                    }, true);
                }
            };
        })
        .directive('focusMe', function ($timeout) {
            return {
                link: function (scope, element, attrs, model) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            };
        });